import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  CLIENT_ID,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  LOGIN_URL,
  ISSUER_URL,
  APP_URL,
  AUTH_SERVER_URL,
  LOGGED_IN,
  CALLBACK_ENDPOINT,
  SILENT_REFRESH_ENDPOINT,
  TOKEN,
  SCOPES_OPENID_ALL,
  STATE,
} from './constants/storage';
import { StorageService } from './api/storage/storage.service';

@Injectable()
export class AppService {
  messageUrl = '/api/info'; // URL to web api

  constructor(
    private http: HttpClient,
    private readonly storage: StorageService,
  ) {}

  /** GET message from the server */
  getMessage(): Observable<any> {
    return this.http.get<any>(this.messageUrl);
  }

  setInfoLocalStorage(response) {
    this.storage
      .setItem(CLIENT_ID, response.frontendClientId)
      .then(() =>
        this.storage.setItem(REDIRECT_URI, response.appURL + CALLBACK_ENDPOINT),
      )
      .then(() =>
        this.storage.setItem(
          SILENT_REFRESH_REDIRECT_URI,
          response.appURL + SILENT_REFRESH_ENDPOINT,
        ),
      )
      .then(() => this.storage.setItem(LOGIN_URL, response.authorizationURL))
      .then(() => this.storage.setItem(ISSUER_URL, response.authServerURL))
      .then(() => this.storage.setItem(APP_URL, response.appURL))
      .then(() => this.storage.setItem(AUTH_SERVER_URL, response.authServerURL))
      .then(() => this.storage.setItem(LOGGED_IN, false))
      .then(saved => {});
  }

  generateRandomString(length: number) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  getStorage() {
    return this.storage;
  }

  setupImplicitFlow(): void {
    this.getMessage().subscribe({
      next: response => {
        if (
          !response ||
          (response &&
            !response.frontendClientId &&
            !response.appURL &&
            !response.authorizationURL)
        ) {
          return;
        }

        this.setInfoLocalStorage(response);
        const frappe_auth_config = {
          client_id: response.frontendClientId,
          redirect_uri: response.appURL + CALLBACK_ENDPOINT,
          response_type: TOKEN,
          scope: SCOPES_OPENID_ALL,
        };
        this.initiateLogin(response.authorizationURL, frappe_auth_config);
        return;
      },
      error: error => {},
    });
  }

  initiateLogin(authorizationUrl: string, frappe_auth_config) {
    const state = this.generateRandomString(32);
    this.getStorage()
      .setItem(STATE, state)
      .then(savedState => {
        window.location.href = this.getEncodedFrappeLoginUrl(
          authorizationUrl,
          frappe_auth_config,
          savedState,
        );
      });
  }

  getEncodedFrappeLoginUrl(authorizationUrl, frappe_auth_config, state) {
    authorizationUrl += `?client_id=${frappe_auth_config.client_id}`;
    authorizationUrl += `&scope=${encodeURIComponent(
      frappe_auth_config.scope,
    )}`;
    authorizationUrl += `&redirect_uri=${encodeURIComponent(
      frappe_auth_config.redirect_uri,
    )}`;
    authorizationUrl += `&response_type=${frappe_auth_config.response_type}`;
    authorizationUrl += `&state=${state}`;
    return authorizationUrl;
  }
}
