import { Component, OnInit } from '@angular/core';
import {
  ACCESS_TOKEN,
  AUTH_SERVER_URL,
  LOGGED_IN,
  AUTHORIZATION,
  BEARER_TOKEN_PREFIX,
} from '../constants/storage';
import { AppService } from '../app.service';
import { Router, NavigationEnd } from '@angular/router';
import { StorageService } from '../api/storage/storage.service';
import { LoginService } from '../api/login/login.service';
import { HttpClient } from '@angular/common/http';
import { filter, map, delay, switchMap } from 'rxjs/operators';
import { from } from 'rxjs';
import { IDTokenClaims } from '../common/interfaces/id-token-claims.interfaces';
import { DIRECT_PROFILE_ENDPOINT } from '../constants/url-strings';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  loggedIn: boolean;
  picture: string;
  state: string;
  email: string;
  fullName: string;
  spinner = true;

  constructor(
    private readonly http: HttpClient,
    private readonly login: LoginService,
    private readonly storage: StorageService,
    private readonly router: Router,
    private readonly appService: AppService,
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(event => {
          this.spinner = true;
          this.loadProfile();
          return event;
        }),
      )
      .subscribe({
        next: res => {
          this.storage.getItem(AUTH_SERVER_URL).then(url => {
            if (!url) {
              this.appService.getMessage().subscribe({
                next: success => this.appService.setInfoLocalStorage(success),
                error: error => {},
              });
            }
          });
        },
        error: err => {},
      });
    this.setUserSession();
    this.loadProfile();
  }

  setUserSession() {
    this.login.changes.subscribe({
      next: event => {
        if (event.key === LOGGED_IN && event.value === false) {
          this.loggedIn = false;
        }
      },
      error: error => {},
    });
  }

  loadProfile() {
    from(this.storage.getItem(ACCESS_TOKEN))
      .pipe(
        delay(1000),
        switchMap(token => {
          this.loggedIn = token ? true : false;
          const headers = {
            [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
          };

          return this.http.get<IDTokenClaims>(DIRECT_PROFILE_ENDPOINT, {
            headers,
          });
        }),
      )
      .subscribe({
        error: error => this.appService.setupImplicitFlow(),
        next: profile => {
          this.spinner = false;
          this.email = profile.email;
          this.fullName = profile.name;
        },
      });
  }
}
